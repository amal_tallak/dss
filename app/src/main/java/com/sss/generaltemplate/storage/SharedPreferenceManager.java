package com.sss.generaltemplate.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sss.generaltemplate.model.User;
import com.sss.generaltemplate.utils.Global;

import static com.sss.generaltemplate.utils.Flags.DEFAULT_LANGUAGE;
import static com.sss.generaltemplate.utils.Flags.LOGGED_IN_STATUS_PREF;
import static com.sss.generaltemplate.utils.Flags.LOGGED_IN_USER;
import static com.sss.generaltemplate.utils.Flags.LOGGED_IN_USER_NAME;
import static com.sss.generaltemplate.utils.Flags.LOGGED_IN_USER_PASSWORD;
import static com.sss.generaltemplate.utils.Flags.MY_LIST;
import static com.sss.generaltemplate.utils.Flags.TOKEN;


public class SharedPreferenceManager {

    public static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void startSession(Context context, String userName, String userPassword, String user, String token) {
        Global.token = token;
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(LOGGED_IN_STATUS_PREF, true);
        editor.putString(LOGGED_IN_USER_NAME, userName);
        editor.putString(LOGGED_IN_USER_PASSWORD, userPassword);
        editor.putString(LOGGED_IN_USER, user);
        editor.putString(TOKEN,Global.token);
        editor.apply();
    }

    public static void endSession(Context context) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(LOGGED_IN_STATUS_PREF, false);
        editor.apply();
    }

    public static boolean getLoggedStatus(Context context) {
        return getPreferences(context).getBoolean(LOGGED_IN_STATUS_PREF, false);
    }

    public static void setLang(Context context, String lang) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(DEFAULT_LANGUAGE, lang);
        editor.apply();
    }

    public static void refreshDefaultLang(Context context) {
        Global.lang = getPreferences(context).getString(DEFAULT_LANGUAGE, "en");
    }


    public static void setMyList(Context context, String myListText) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(MY_LIST, myListText);
        editor.apply();
    }

    public static String getMyList(Context context) {
        return getPreferences(context).getString(MY_LIST, "");
    }



    public static User getCurrentUser(Context context) {
        String userStr = getPreferences(context).getString(LOGGED_IN_USER, "");
        if (userStr.equals("")) return null;
        else {
            JsonParser jsonParser = new JsonParser();
            JsonObject userJson = (JsonObject) jsonParser.parse(userStr);
            User user = User.getUserFromJson(userJson);
            return user;
        }
    }

    public static String getToken(Context context){
        String token = getPreferences(context).getString(TOKEN,"");
        return token;
    }
}
