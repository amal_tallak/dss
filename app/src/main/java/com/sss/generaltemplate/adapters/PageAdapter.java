package com.sss.generaltemplate.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sss.generaltemplate.R;
import com.sss.generaltemplate.fragments.FragmentShowChart;
import com.sss.generaltemplate.fragments.FragmentShowTable;
import com.sss.generaltemplate.model.FactTable;

import java.util.ArrayList;


public class PageAdapter extends FragmentPagerAdapter {

    int numOfTabs = 2;
    FactTable factTable;
    ArrayList<String> dimens;
    Context context;

    public PageAdapter(FragmentManager fm, Context context, FactTable factTable, ArrayList<String> dimens) {
        super(fm);
        this.context = context;
        this.factTable=factTable;
        this.dimens=dimens;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentShowChart(factTable,dimens);
            case 1:
                return new FragmentShowTable();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    public String getPageTitle(int position) {
        if (position == 0)
            return context.getResources().getString(R.string.show_chart);
        else if (position == 1)
            return context.getResources().getString(R.string.show_table);
        else
            return null;
    }
}
