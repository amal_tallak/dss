package com.sss.generaltemplate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;

import com.sss.generaltemplate.R;

import java.util.List;

public class SeekBarAdapter extends ArrayAdapter<Integer> {

    List<Integer> data;
    int resource;

    public SeekBarAdapter(Context context, int layoutResource, List<Integer> data){
        super(context, layoutResource, data);
        this.data = data;
        this.resource = layoutResource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        Integer value = data.get(position);

        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource, parent);
        }

        SeekBar sb = convertView.findViewById(R.id.seekBar);
        sb.setProgress(value);

        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean wasInitiatedByUser) {
                // do what you want with the edited values here.
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return convertView;
    }
}
