package com.sss.generaltemplate.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sss.generaltemplate.R;
import com.sss.generaltemplate.fragments.FragmentShowChart;

import java.util.ArrayList;

public class YearsAdapter extends RecyclerView.Adapter<YearsAdapter.MyViewHolder> {
    FragmentShowChart fragmentShowChart;
    public ArrayList<Integer> listYear;
    Context context;
    View.OnClickListener mOnItemClickListener;

    public YearsAdapter(ArrayList<Integer> listYear) {
        this.listYear = listYear;
    }

//    @Override
//    public YearsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//        Context context = viewGroup.getContext();
//        LayoutInflater inflater = LayoutInflater.from(context);
//
//        return new YearsAdapter.ViewHolder(context, inflater.inflate(R.layout.list_years, viewGroup, false));
//    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_years, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvYear.setText(listYear.get(position).toString());
    }

//    @Override
//    public void onBindViewHolder(final YearsAdapter.ViewHolder viewHolder, final int i) {
//        viewHolder.tvYear.setText(listYear.get(i).toString());
//        viewHolder.bind(listYear.get(i), onItemClickListener);
////        viewHolder.tvYear.setOnClickListener(new View.OnClickListener() {
////            @SuppressLint("ResourceAsColor")
////            @Override
////            public void onClick(View v) {
////                fragmentShowChart.year=listYear.get(i);
////                for(int j=0;j<listYear.size();j++){
////                    if(j!=i)
////                        viewHolder.tvYear.setTextColor(context.getResources().getColor(R.color.goldColor));
////                }
//////                new FragmentShowChart.MyCountDown(listYear.size() * 3000, 3000).start();
////                viewHolder.tvYear.setTextColor(context.getResources().getColor(R.color.maroonColor));
////                fragmentShowChart.buildChart();
////            }
////        });
//
//    }


    @Override
    public int getItemCount() {
        return listYear.size();
    }

//    public class ViewHolder extends RecyclerView.ViewHolder {
//        TextView tvYear;
//        private Context context;
//
//
//        ViewHolder(Context context, View itemView) {
//            super(itemView);
//            this.context = context;
//            tvYear = itemView.findViewById(R.id.year);
// }
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvYear;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvYear = itemView.findViewById(R.id.year);
            itemView.setTag(this);
            itemView.setOnClickListener(mOnItemClickListener);
        }
        }


    public void setOnItemClickListener(View.OnClickListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }

}
