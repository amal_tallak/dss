package com.sss.generaltemplate.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sss.generaltemplate.R;
import com.sss.generaltemplate.activities.MainActivity;
import com.sss.generaltemplate.fragments.FragmentStatistic;
import com.sss.generaltemplate.model.ReportTable;
import com.sss.generaltemplate.model.StatisticReport;

public class StatisticReportsAdapter extends RecyclerView.Adapter<StatisticReportsAdapter.ViewHolder> {
    StatisticReport[] statisticReports;
    Context context;
    ReportTable[] reportTables;
    FragmentStatistic fragmentStatistic;


    public StatisticReportsAdapter(Context context, StatisticReport[] statisticReports) {
        this.context = context;
        this.statisticReports = statisticReports;
    }

    @Override
    public StatisticReportsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new StatisticReportsAdapter.ViewHolder(context, inflater.inflate(R.layout.list_statistic_reports, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(StatisticReportsAdapter.ViewHolder viewHolder, int i) {
        StatisticReport statisticReport = statisticReports[i];
        TextView textView = viewHolder.tvName;
        textView.setText(statisticReport.getName());
    }

    @Override
    public int getItemCount() {
        return statisticReports.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName;
        private Context context;


        ViewHolder(Context context, View itemView) {
            super(itemView);

            this.context = context;
            tvName = itemView.findViewById(R.id.subject);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            reportTables = statisticReports[position].getStatisticReportTables();

            fragmentStatistic = new FragmentStatistic(reportTables);
            ((MainActivity) context).setFragment(fragmentStatistic);
        }


    }
}
