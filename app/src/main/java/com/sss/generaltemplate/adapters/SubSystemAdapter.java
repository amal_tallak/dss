package com.sss.generaltemplate.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sss.generaltemplate.R;
import com.sss.generaltemplate.activities.MainActivity;
import com.sss.generaltemplate.fragments.FragmentStatisticReports;
import com.sss.generaltemplate.model.DashboardItem;
import com.sss.generaltemplate.model.StatisticReport;
import com.sss.generaltemplate.model.SubSystem;

public class SubSystemAdapter extends RecyclerView.Adapter<SubSystemAdapter.ViewHolder> {
    FragmentStatisticReports fragmentStatisticReports;
    DashboardItem[] dashboardItems;
    Context context;
    StatisticReport[] statisticReports;

    public SubSystemAdapter(Context context, DashboardItem[] dashboardItems) {
        this.context = context;
        this.dashboardItems = dashboardItems;
    }

    @Override
    public SubSystemAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new SubSystemAdapter.ViewHolder(context, inflater.inflate(R.layout.list_sub_system, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(SubSystemAdapter.ViewHolder viewHolder, final int i) {
        DashboardItem item = dashboardItems[i];
        for (int j = 0; j < item.getItems().size(); j++) {
            final SubSystem subSystem = item.getItems().get(j);
            viewHolder.getTextViewByIndex(j).setText(subSystem.getName());
            viewHolder.getRelativeLayoutByIndex(j).setVisibility(View.VISIBLE);
            viewHolder.getRelativeLayoutByIndex(j).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    statisticReports = subSystem.getStatisticReports();
                    fragmentStatisticReports = new FragmentStatisticReports(statisticReports);
                    ((MainActivity) context).setFragment(fragmentStatisticReports);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dashboardItems.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName1, tvName2, tvName3, tvName4, tvName5, tvName6;
        RelativeLayout layout1, layout2, layout3, layout4, layout5, layout6;
        private Context context;


        ViewHolder(Context context, View itemView) {
            super(itemView);

            this.context = context;
            tvName1 = itemView.findViewById(R.id.subject1);
            tvName2 = itemView.findViewById(R.id.subject2);
            tvName3 = itemView.findViewById(R.id.subject3);
            tvName4 = itemView.findViewById(R.id.subject4);
            tvName5 = itemView.findViewById(R.id.subject5);
            tvName6 = itemView.findViewById(R.id.subject6);
            layout1 = itemView.findViewById(R.id.rL1);
            layout2 = itemView.findViewById(R.id.rL2);
            layout3 = itemView.findViewById(R.id.rL3);
            layout4 = itemView.findViewById(R.id.rL4);
            layout5 = itemView.findViewById(R.id.rL5);
            layout6 = itemView.findViewById(R.id.rL6);
        }

        TextView getTextViewByIndex(int index) {
            switch (index) {
                case 0:
                    return tvName1;
                case 1:
                    return tvName2;
                case 2:
                    return tvName3;
                case 3:
                    return tvName4;
                case 4:
                    return tvName5;
                case 5:
                    return tvName6;
                default:
                    return null;
            }
        }

        RelativeLayout getRelativeLayoutByIndex(int index) {
            switch (index) {
                case 0:
                    return layout1;
                case 1:
                    return layout2;
                case 2:
                    return layout3;
                case 3:
                    return layout4;
                case 4:
                    return layout5;
                case 5:
                    return layout6;
                default:
                    return null;
            }
        }
    }
}
