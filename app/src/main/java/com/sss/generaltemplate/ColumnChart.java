package com.sss.generaltemplate;

import android.view.View;

import com.highsoft.highcharts.common.hichartsclasses.HICSSObject;
import com.highsoft.highcharts.common.hichartsclasses.HIColumn;
import com.highsoft.highcharts.common.hichartsclasses.HICrosshair;
import com.highsoft.highcharts.common.hichartsclasses.HILabels;
import com.highsoft.highcharts.common.hichartsclasses.HIOptions;
import com.highsoft.highcharts.common.hichartsclasses.HIPlotOptions;
import com.highsoft.highcharts.common.hichartsclasses.HISeries;
import com.highsoft.highcharts.common.hichartsclasses.HITitle;
import com.highsoft.highcharts.common.hichartsclasses.HIXAxis;
import com.highsoft.highcharts.common.hichartsclasses.HIYAxis;
import com.highsoft.highcharts.core.HIChartView;

import java.util.ArrayList;

public class ColumnChart {

    View view;
    ArrayList<String> arrayListX;
    ArrayList<Integer> arrayListY = new ArrayList<>();
    ArrayList<String> arrayListZ;
    HIChartView chartView;
    HIOptions options;
    ArrayList<ArrayList<Integer>> valuesXZ = new ArrayList<ArrayList<Integer>>();

    public ColumnChart() {

    }

    public ColumnChart(HIChartView chartView, HIOptions options) {
        this.chartView = chartView;
        this.options = options;
    }


    public void viewChart(ArrayList arrayListX, ArrayList arrayListY, ArrayList arrayListZ, ArrayList<String> finalDimens) {
        buildChartLists(arrayListX, arrayListY, arrayListZ);
        prepareChart();
        chartView.setVisibility(View.VISIBLE);
        ArrayList series = new ArrayList<>();

        if (arrayListZ.size() != 0) {
            for (int i = 0; i < this.arrayListZ.size(); i++) {
                HISeries series1 = new HIColumn();
                series1.setName((this.arrayListZ.get(i)));
                this.arrayListY = new ArrayList<>();
                for (int j = 0; j < this.arrayListX.size(); j++) {
                    this.arrayListY.add(valuesXZ.get(j).get(i));
                }
                series1.setData(this.arrayListY);
                series.add(series1);
            }
        } else {
            HISeries series1 = new HIColumn();
            series1.setName("");
            series1.setData(this.arrayListY);
            series.add(series1);
        }
        options.setSeries(series);

        chartView.setOptions(options);

        chartView.reload();
    }

    public void buildChartLists(ArrayList arrayListX, ArrayList arrayListY, ArrayList arrayListZ) {
        this.arrayListX = new ArrayList();
        for (int i = 0; i < arrayListX.size(); i++) {
            if (this.arrayListX.indexOf(arrayListX.get(i)) == -1) {
                this.arrayListX.add(arrayListX.get(i).toString());
            }
        }
        if (arrayListZ.size() != 0) {
            this.arrayListZ = new ArrayList();
            for (int i = 0; i < arrayListZ.size(); i++) {
                if (this.arrayListZ.indexOf(arrayListZ.get(i)) == -1) {
                    this.arrayListZ.add(arrayListZ.get(i).toString());
                }
            }
        }
        for (int i = 0; i < arrayListY.size(); i++) {
            if (arrayListZ.size() != 0) {
                int indexX = this.arrayListX.indexOf(arrayListX.get(i));
                int indexZ = this.arrayListZ.indexOf(arrayListZ.get(i));
                try {
                    ArrayList<Integer> temp = new ArrayList<>();
                    temp.add(this.valuesXZ.get(indexX).get(indexZ));
                    this.valuesXZ.get(indexX).set(indexZ, temp.get(indexZ) + Integer.parseInt(arrayListY.get(i).toString()));
                } catch (IndexOutOfBoundsException e) {
                    try {
                        this.valuesXZ.get(indexX).add(indexZ, Integer.parseInt(arrayListY.get(i).toString()));
                    } catch (IndexOutOfBoundsException e1) {
                        this.valuesXZ.add(indexX, new ArrayList<Integer>());
                        this.valuesXZ.get(indexX).add(indexZ, Integer.parseInt(arrayListY.get(i).toString()));
                    }
                }
            } else {
                int indexX = this.arrayListX.indexOf(arrayListX.get(i));
                try {
                    Integer tempX = this.arrayListY.get(indexX);
                    this.arrayListY.set(indexX, tempX + Integer.parseInt(arrayListY.get(i).toString()));

                } catch (IndexOutOfBoundsException e) {
                    this.arrayListY.add(indexX, Integer.parseInt(arrayListY.get(i).toString()));
                }
            }
        }
    }

    public void prepareChart() {
        final HIXAxis xAxis = new HIXAxis();
        xAxis.setCategories(this.arrayListX);
        xAxis.setCrosshair(new HICrosshair());
        xAxis.setLabels(new HILabels());
        xAxis.getLabels().setSkew3d(true);
        xAxis.getLabels().setStyle(new HICSSObject());
        xAxis.getLabels().getStyle().setFontSize("16px");
        options.setXAxis(new ArrayList<HIXAxis>() {{
            add(xAxis);
        }});

        final HIYAxis yAxis = new HIYAxis();
        yAxis.setMin(0);
        yAxis.setTitle(new HITitle());
        yAxis.getTitle().setText("counts");
        options.setYAxis(new ArrayList<HIYAxis>() {{
            add(yAxis);
        }});


        HIPlotOptions plotOptions = new HIPlotOptions();
        plotOptions.setColumn(new HIColumn());
        plotOptions.getColumn().setDepth(25);
        plotOptions.getColumn().setGroupZPadding(10);

        options.setPlotOptions(plotOptions);
    }
}
