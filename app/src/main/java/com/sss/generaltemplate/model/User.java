package com.sss.generaltemplate.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User implements Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("domainName")
    @Expose
    private String domainName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("systemName")
    @Expose
    private String systemName;


    public static User getUserFromJson(JsonElement jsonElement) {
        Gson gson = new Gson();
        User customer = gson.fromJson(jsonElement, User.class);
        return customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}