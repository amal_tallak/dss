package com.sss.generaltemplate.model;

import java.util.ArrayList;
import java.util.List;

public class DashboardItem {

    private List<SubSystem> items = new ArrayList<>();

    public DashboardItem() {
    }

    public List<SubSystem> getItems() {
        return items;
    }

    public void setItems(List<SubSystem> items) {
        this.items = items;
    }
}
