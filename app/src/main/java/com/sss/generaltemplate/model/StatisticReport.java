package com.sss.generaltemplate.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sss.generaltemplate.utils.Global;

public class StatisticReport implements Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("arabicName")
    @Expose
    private String arabicName;
    @SerializedName("englishName")
    @Expose
    private String englishName;
    @SerializedName("statisticReportTables")
    @Expose
    private ReportTable[] statisticReportTables;


    public static StatisticReport getUserFromJson(JsonElement jsonElement) {
        Gson gson = new Gson();
        StatisticReport customer = gson.fromJson(jsonElement, StatisticReport.class);
        return customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public ReportTable[] getStatisticReportTables() {
        return statisticReportTables;
    }

    public void setStatisticReportTables(ReportTable[] statisticReportTables) {
        this.statisticReportTables = statisticReportTables;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getName() {
        if (Global.lang.equals("en")) return this.englishName;
        else return this.arabicName;
    }
}