package com.sss.generaltemplate.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportTable implements Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("statisticTable")
    @Expose
    private StatisticTable statisticTable;
    @SerializedName("statisticReport")
    @Expose
    private StatisticReport statisticReport;

    public static ReportTable getUserFromJson(JsonElement jsonElement) {
        Gson gson = new Gson();
        ReportTable customer = gson.fromJson(jsonElement, ReportTable.class);
        return customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StatisticTable getStatisticTable() {
        return statisticTable;
    }

    public void setStatisticTable(StatisticTable statisticTable) {
        this.statisticTable = statisticTable;
    }

    public StatisticReport getStatisticReport() {
        return statisticReport;
    }

    public void setStatisticReport(StatisticReport statisticReport) {
        this.statisticReport = statisticReport;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}