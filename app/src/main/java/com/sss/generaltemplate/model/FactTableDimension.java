package com.sss.generaltemplate.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FactTableDimension implements Cloneable {

    @SerializedName("dimension")
    @Expose
    private Dimension dimension;
    @SerializedName("factTable")
    @Expose
    private FactTable factTable;


    public static FactTableDimension getUserFromJson(JsonElement jsonElement) {
        Gson gson = new Gson();
        FactTableDimension customer = gson.fromJson(jsonElement, FactTableDimension.class);
        return customer;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public FactTable getFactTable() {
        return factTable;
    }

    public void setFactTable(FactTable factTable) {
        this.factTable = factTable;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}