package com.sss.generaltemplate.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FactTable implements Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("arabicName")
    @Expose
    private String arabicName;
    @SerializedName("englishName")
    @Expose
    private String englishName;
    @SerializedName("timeField")
    @Expose
    private String timeField;
    @SerializedName("recourdCountField")
    @Expose
    private String recourdCountField;
    @SerializedName("factTableDimensions")
    @Expose
    private FactTableDimension[] factTableDimensions;


    public static FactTable getUserFromJson(JsonElement jsonElement) {
        Gson gson = new Gson();
        FactTable customer = gson.fromJson(jsonElement, FactTable.class);
        return customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getTimeField() {
        return timeField;
    }

    public void setTimeField(String timeField) {
        this.timeField = timeField;
    }

    public String getRecourdCountField() {
        return recourdCountField;
    }

    public void setRecourdCountField(String recourdCountField) {
        this.recourdCountField = recourdCountField;
    }

    public FactTableDimension[] getFactTableDimensions() {
        return factTableDimensions;
    }

    public void setFactTableDimensions(FactTableDimension[] factTableDimensions) {
        this.factTableDimensions = factTableDimensions;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}