package com.sss.generaltemplate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sss.generaltemplate.utils.Global;

public class Dimension implements Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("arabicName")
    @Expose
    private String arabicName;
    @SerializedName("englishName")
    @Expose
    private String englishName;
    @SerializedName("codeingSource")
    @Expose
    private String codeingSource;
    @SerializedName("codeId")
    @Expose
    private String codeId;
    @SerializedName("codeArabicName")
    @Expose
    private String codeArabicName;
    @SerializedName("codeEnglishName")
    @Expose
    private String codeEnglishName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getCodeingSource() {
        return codeingSource;
    }

    public void setCodeingSource(String codeingSource) {
        this.codeingSource = codeingSource;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }

    public String getCodeArabicName() {
        return codeArabicName;
    }

    public void setCodeArabicName(String codeArabicName) {
        this.codeArabicName = codeArabicName;
    }

    public String getCodeEnglishName() {
        return codeEnglishName;
    }

    public void setCodeEnglishName(String codeEnglishName) {
        this.codeEnglishName = codeEnglishName;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


    public String getName() {
        if (Global.lang.equals("en")) return this.englishName;
        else return this.arabicName;
    }
}