package com.sss.generaltemplate.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatisticTableDimension implements Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("dimension")
    @Expose
    private Dimension dimension;


    public static StatisticTableDimension getUserFromJson(JsonElement jsonElement) {
        Gson gson = new Gson();
        StatisticTableDimension customer = gson.fromJson(jsonElement, StatisticTableDimension.class);
        return customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}