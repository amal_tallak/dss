package com.sss.generaltemplate.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sss.generaltemplate.utils.Global;

public class SubSystem implements Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("arabicName")
    @Expose
    private String arabicName;
    @SerializedName("englishName")
    @Expose
    private String englishName;
    @SerializedName("statisticReports")
    @Expose
    private StatisticReport[] statisticReports;


    public static SubSystem[] getSubSystemFromJson(JsonElement jsonElement) {
        Gson gson = new Gson();
        SubSystem []subSystems = gson.fromJson(jsonElement, SubSystem[].class);
        return subSystems;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public StatisticReport[] getStatisticReports() {
        return statisticReports;
    }

    public void setStatisticReports(StatisticReport[] statisticReports) {
        this.statisticReports = statisticReports;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getName() {
        if (Global.lang.equals("en"))return this.englishName;
        else return this.arabicName;}
}