package com.sss.generaltemplate.activities;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sss.generaltemplate.R;
import com.sss.generaltemplate.model.User;
import com.sss.generaltemplate.services.AuthenticationsService;
import com.sss.generaltemplate.storage.SharedPreferenceManager;
import com.sss.generaltemplate.utils.APIUtils;
import com.sss.generaltemplate.utils.FingerprintHandler;
import com.sss.generaltemplate.utils.Global;
import com.sss.generaltemplate.utils.LoaderUtils;
import com.sss.generaltemplate.utils.MyApplication;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sss.generaltemplate.utils.Flags.LOGGED_IN_USER_NAME;


public class LoginActivity extends AppCompatActivity {

    EditText emailOrMobileEditText;
    EditText passwordEditText;
    TextView forgotPasswordTextView;
    Button loginButton;
    // private static DataBaseHelper databaseHelper;
    private static AuthenticationsService authenticationsService;
    private static final String KEY_NAME = "yourKey";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    public static String userName;
    public static String userPassword;
    private static Activity currentContext;
    Switch langSwitch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        MyApplication.changeLang(this);

        this.initLayoutElements();
        this.initServices();

        Global.initGsonBuilder();
        if (SharedPreferenceManager.getLoggedStatus(getApplicationContext())) {
            Global.currentUser = SharedPreferenceManager.getCurrentUser(getApplicationContext());
            Global.token=SharedPreferenceManager.getToken(getApplicationContext());
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }

    public void initServices() {
        this.authenticationsService = APIUtils.getAuthenticationsService();
    }

    public void initLayoutElements() {
        currentContext = LoginActivity.this;
        emailOrMobileEditText = (EditText) findViewById(R.id.emailOrMobileNumber);
        passwordEditText = (EditText) findViewById(R.id.password);
        loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
        this.integrationWithFingerprint();
    }

    public void swichLang(final Context context) {
        this.langSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Global.lang.equals("en")) {
                    Global.lang = "ar";
                    SharedPreferenceManager.setLang(getApplicationContext(), "ar");
                    Intent i = new Intent(getApplicationContext(), context.getClass());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } else {
                    Global.lang = "en";
                    SharedPreferenceManager.setLang(getApplicationContext(), "en");
                    Intent i = new Intent(getApplicationContext(), context.getClass());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        });
    }
    public void validate() {
        if (emailOrMobileEditText.getText().toString().trim().equals("")
                || passwordEditText.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), R.string.enter_required_fields, Toast.LENGTH_LONG).show();
        } else {
            User user = new User();
            user.setDomainName(emailOrMobileEditText.getText().toString().trim());
            user.setPassword(passwordEditText.getText().toString().trim());
            user.setSystemName(Global.systemName);
            userName = user.getDomainName();
            userPassword = user.getPassword();
            this.login(user);
        }
    }

    public static void login(final User user) {
        LoaderUtils.show(currentContext);
        authenticationsService.login(user).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                LoaderUtils.hide();
                if (response.isSuccessful()) {
                    if (((boolean) response.body().getAsJsonObject().get("message").getAsJsonObject().get("state").toString().contains("SUCCESS"))) {
                        User currentUser = User.getUserFromJson(response.body().getAsJsonObject().get("user").getAsJsonObject());
                        Global.token = response.headers().get("Authorization");
                        SharedPreferenceManager.startSession(currentContext, userName, userPassword, response.body().getAsJsonObject().get("user").toString(), Global.token);
                        Global.currentUser = currentUser;
                        Intent i = new Intent(currentContext, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        currentContext.startActivity(i);
                    } else {
                        try {
                            JsonObject errors = response.body().getAsJsonObject().getAsJsonObject("errors");
                            for (String s : errors.keySet()) {
                                String packageName = currentContext.getPackageName();
                                Integer resId = currentContext.getApplicationContext().getResources().
                                        getIdentifier("_" + errors.get(s).getAsString(),
                                                "string", packageName);
                                String res = currentContext.getResources().getString(resId) + " - " + errors.get(s).getAsString();
                                Toast.makeText(currentContext.getApplicationContext(), res, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                LoaderUtils.hide();
                String lastUser = SharedPreferenceManager.getPreferences(currentContext)
                        .getString(LOGGED_IN_USER_NAME, "");
                if (lastUser.length() > 0) {
//                    SharedPreferenceManager.startSession(currentContext, userName, userPassword,response.body().toString(), Global.token);
                    //User currentUser = databaseHelper.getCustomerInfos().get(0);
                    //Global.currentUser = currentUser;
                    Intent i = new Intent(currentContext, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    currentContext.startActivity(i);
                }
            }
        });
    }
    public void integrationWithFingerprint(){
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //Get an instance of KeyguardManager and FingerprintManager//
                keyguardManager =
                        (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                fingerprintManager =
                        (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                //Check whether the device has a fingerprint sensor//
                if (!fingerprintManager.isHardwareDetected()) {
                    // If a fingerprint sensor isn’t available, then inform the user that they’ll be unable to use your app’s fingerprint functionality//
                }
                //Check whether the user has granted your app the USE_FINGERPRINT permission//
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    // If your app doesn't have this permission, then display the following text//
                }

                //Check that the user has registered at least one fingerprint//
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    // If the user hasn’t configured any fingerprints, then display the following message//
                }

                //Check that the lockscreen is secured//
                if (!keyguardManager.isKeyguardSecure()) {
                    // If the user hasn’t secured their lockscreen with a PIN password or pattern, then display the following text//
                } else {
                    try {
                        generateKey();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (initCipher()) {
                        //If the cipher is initialized successfully, then create a CryptoObject instance//
                        cryptoObject = new FingerprintManager.CryptoObject(cipher);

                        // Here, I’m referencing the FingerprintHandler class that we’ll create in the next section. This class will be responsible
                        // for starting the authentication process (via the startAuth method) and processing the authentication process events//
                        FingerprintHandler helper = new FingerprintHandler(this);
                        helper.startAuth(fingerprintManager, cryptoObject);
                    }
                }
            }
        } catch (Exception e) {
            // Device greater than Android.M and it is not has Fingerprint
            e.printStackTrace();
        }
    }
    //Create the generateKey method that we’ll use to gain access to the Android keystore and generate the encryption key//

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKey() {
        try {
            // Obtain a reference to the Keystore using the standard Android keystore container identifier (“AndroidKeystore”)//
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            //Generate the key//
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            //Initialize an empty KeyStore//
            keyStore.load(null);

            //Initialize the KeyGenerator//
            keyGenerator.init(new
                    //Specify the operation(s) this key can be used for//
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)

                    //Configure this key so that the user has to confirm their identity with a fingerprint each time they want to use it//
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            //Generate the key//
            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
        }
    }

    //Create a new method that we’ll use to initialize our cipher//
    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean initCipher() {
        try {
            //Obtain a cipher instance and configure it with the properties required for fingerprint authentication//
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            e.printStackTrace();
            return false;
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //Return true if the cipher has been initialized successfully//
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {

            //Return false if cipher initialization failed//
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return false;
        }
    }
}
