package com.sss.generaltemplate.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.sss.generaltemplate.R;
import com.sss.generaltemplate.fragments.FragmentDashboard;
import com.sss.generaltemplate.storage.SharedPreferenceManager;
import com.sss.generaltemplate.utils.Global;
import com.sss.generaltemplate.utils.MyApplication;

public class MainActivity extends AppCompatActivity {

    FragmentDashboard fragmentDashboard;
    Fragment currentFragment;
    NavigationView navigationView;
    DrawerLayout mDrawerLayout;
    private Switch langSwitch;
    TextView pageTitle;
    public ImageView bottomPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initLayoutElements();
        navigationMenu();
//        swichLang(this);

        if (savedInstanceState != null) {
            currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_frame);
            setFragment(currentFragment);
        } else {
            setFragment(fragmentDashboard);
        }

    }

    public void initLayoutElements() {
        pageTitle = (TextView) findViewById(R.id.menu_page_title);
        fragmentDashboard = new FragmentDashboard();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        bottomPage = (ImageView) findViewById(R.id.bottom_page);
//        langSwitch = (Switch) headerView.findViewById(R.id.navLangSwitch);
    }

    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void navigationMenu() {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();

                        switch (menuItem.getItemId()) {

                            case R.id.home:
                                MyApplication.changeLang(MainActivity.this);
                                setFragment(fragmentDashboard);
                                return true;

//                            case R.id.changeDept:
//                                MyApplication.changeLang(MainActivity.this);
//                                showDepartmentDialog();
//                                return true;

                            case R.id.nav_logout:
                                SharedPreferenceManager.endSession(getApplicationContext());
                                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                return true;
                        }

                        return true;
                    }
                });

    }

    public void swichLang(final Context context) {
        this.langSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Global.lang.equals("en")) {
                    Global.lang = "ar";
                    SharedPreferenceManager.setLang(getApplicationContext(), "ar");
                    Intent i = new Intent(getApplicationContext(), context.getClass());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } else {
                    Global.lang = "en";
                    SharedPreferenceManager.setLang(getApplicationContext(), "en");
                    Intent i = new Intent(getApplicationContext(), context.getClass());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Fragment currentBackStackFragment = getSupportFragmentManager().findFragmentById(R.id.main_frame);
        if (currentBackStackFragment instanceof FragmentDashboard) {

            new AlertDialog.Builder(this)
                    .setTitle(R.string.closeApp)
                    .setMessage(R.string.closeAppMsg)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                finishAffinity();
                                System.exit(0);
                            }

                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        } else {
            super.onBackPressed();
        }
    }

    public void setPageTitle(String title) {
        pageTitle.setText(title);
    }
}

