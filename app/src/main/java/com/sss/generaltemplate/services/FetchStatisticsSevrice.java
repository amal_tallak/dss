package com.sss.generaltemplate.services;

import com.google.gson.JsonElement;
import com.sss.generaltemplate.model.StatisticReport;
import com.sss.generaltemplate.model.StatisticTable;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FetchStatisticsSevrice {
    @GET("statistics")
    Call<JsonElement> getStatistics(@Query(value = "factTableId") int factTableId,
                                    @Query(value = "dimensions",encoded = true) String dimensions,
                                    @Header(value = "Authorization") String token);

    @POST("statistic-table")
    Call<JsonElement> saveStatisticTable(@Body StatisticTable statisticTable);

    @POST("statistic-report")
    Call<JsonElement> saveStatisticReport(@Body StatisticReport statisticReport);
}



