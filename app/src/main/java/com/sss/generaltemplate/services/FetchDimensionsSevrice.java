package com.sss.generaltemplate.services;

import com.google.gson.JsonElement;
import com.sss.generaltemplate.model.Dimension;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface FetchDimensionsSevrice {
    @GET("dimension/get-all")
    Call<JsonElement> getAll();

    @POST("dimension")
    Call<JsonElement> saveDimension(@Body Dimension dimension);


    }



