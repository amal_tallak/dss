package com.sss.generaltemplate.services;

import com.google.gson.JsonElement;
import com.sss.generaltemplate.model.FactTable;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface FetchFactTablesSevrice {
    @GET("fact-table/get-all")
    Call<JsonElement> getAll();

    @POST("fact-table")
    Call<JsonElement> saveFactTable(@Body FactTable factTable);


    }



