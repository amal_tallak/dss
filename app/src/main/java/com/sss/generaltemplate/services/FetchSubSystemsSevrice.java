package com.sss.generaltemplate.services;

import com.google.gson.JsonElement;
import com.sss.generaltemplate.model.SubSystem;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface FetchSubSystemsSevrice {
    @GET("sub-system/get-all")
    Call<JsonElement> getAll( @Header(value = "Authorization") String token);

    @GET("sub-system/with-reports")
    Call<JsonElement> getReports(@Header(value = "Authorization") String token);

    @POST("sub-system")
    Call<JsonElement> saveSubSystem(@Body SubSystem subSystem);
    }



