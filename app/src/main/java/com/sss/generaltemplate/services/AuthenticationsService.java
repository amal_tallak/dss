package com.sss.generaltemplate.services;

import com.google.gson.JsonElement;
import com.sss.generaltemplate.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthenticationsService {
    @POST("login")
    Call<JsonElement> login(@Body User user);

}
