package com.sss.generaltemplate.services;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FetchGeneralDataSevrice {
    @GET("global-data/get-all")
    Call<JsonElement> getAll();

    }



