package com.sss.generaltemplate.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import com.sss.generaltemplate.storage.SharedPreferenceManager;
import java.util.Locale;
import static com.sss.generaltemplate.utils.Global.lang;


public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
     //   Fabric.with(this, new Crashlytics());
    }

    public static void changeLang(Context context) {
        SharedPreferenceManager.refreshDefaultLang(context);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration config = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(new Locale(lang));
        } else {
            config.locale = new Locale(lang);
        }
        res.updateConfiguration(config, dm);
    }
}