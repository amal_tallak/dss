package com.sss.generaltemplate.utils;


import com.sss.generaltemplate.services.AuthenticationsService;
import com.sss.generaltemplate.services.FetchDimensionsSevrice;
import com.sss.generaltemplate.services.FetchFactTablesSevrice;
import com.sss.generaltemplate.services.FetchGeneralDataSevrice;
import com.sss.generaltemplate.services.FetchStatisticsSevrice;
import com.sss.generaltemplate.services.FetchSubSystemsSevrice;

public class APIUtils {

    private APIUtils() {
    }

    // smartsoft
    public static final String SCM_SERVER_URL = "http://smartsoftqa.ddns.net:3500/SCM-BE/";
    public static final String SERVER_URL = "http://smartsoftqa.ddns.net:3500/DSS/";


    public static AuthenticationsService getAuthenticationsService() {
        return RetrofitClient.getClient(SCM_SERVER_URL).create(AuthenticationsService.class);
    }

    public static FetchSubSystemsSevrice getSubSystemsSevrice() {
        return RetrofitClient.getClient(SERVER_URL).create(FetchSubSystemsSevrice.class);
    }

    public static FetchStatisticsSevrice getStatisticsSevrice() {
        return RetrofitClient.getClient(SERVER_URL).create(FetchStatisticsSevrice.class);
    }

    public static FetchFactTablesSevrice getFactTablesSevrice() {
        return RetrofitClient.getClient(SERVER_URL).create(FetchFactTablesSevrice.class);
    }

    public static FetchDimensionsSevrice getDimensionsSevrice() {
        return RetrofitClient.getClient(SERVER_URL).create(FetchDimensionsSevrice.class);
    }

    public static FetchGeneralDataSevrice getGeneralDataSevrice() {
        return RetrofitClient.getClient(SERVER_URL).create(FetchGeneralDataSevrice.class);
    }
}

