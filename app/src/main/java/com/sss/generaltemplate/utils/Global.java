package com.sss.generaltemplate.utils;

import android.graphics.Bitmap;
import android.location.Location;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.sss.generaltemplate.model.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Stack;


public class Global {
    public static String lang = "en";
    public static Stack<Integer> backNavStack = new Stack<Integer>();
    public static User currentUser;
    public static Bitmap bitmap;
    public static GsonBuilder builder = new GsonBuilder();
    public static Boolean isStorageGranted = false;
    public static Boolean enableNearBy = true;
    public static Integer notificationPage = 0;
    public final static String systemName = "AmanProject";
    public static String token;

    Global() {
    }

    public static void initGsonBuilder() {
        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        });
    }

    public static String convertArrayToString(ArrayList<String> array) {
        String res = "[";
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i) != null && !array.get(i).equals("")) {
                if (!res.equals("[")) {
                    res += ",";
                }
                res += "\"" + array.get(i) + "\"";
            }
        }
        res += "]";
        return res;
    }


    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     * <p>
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     *
     * @returns Distance in Meters
     */
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    public static boolean locationEqual(Location l1, Location l2) {
        if (l1.getLongitude() == l2.getLongitude()
                && l1.getLatitude() == l2.getLatitude()
                && l1.getAltitude() == l2.getAltitude()) {
            return true;
        }
        return false;
    }
}
