package com.sss.generaltemplate.utils;

public class Flags {
    public static final Integer MY_PROFILE = 1;
    public static final Integer FAQ = 2;
    public static final Integer CONTACT_US = 3;
    public static final Integer MEMBER_DETAILS = 4;
    public static final Integer HEAD_OF_HOUSE_HOLD = 5;
    public static final Integer MY_CARD = 6;
    public static final Integer MY_TRANSACTION = 7;
    public static final Integer MAIN = 8;
    public static final String LOGGED_IN_STATUS_PREF = "logged-in-status";
    public static final String LOGGED_IN_USER_NAME = "logged-in-user-name";
    public static final String LOGGED_IN_USER_PASSWORD = "logged-in-user-password";
    public static final String LOGGED_IN_USER = "logged-in-user";
    public static final String TOKEN = "token";
    public static final String SENT_TOKEN_TO_SERVER = "sent-token-to-server";
    public static final String DEFAULT_LANGUAGE = "Lang";
    public static final String MY_LIST = "my-list";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
}
