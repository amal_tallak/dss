package com.sss.generaltemplate.utils;

import android.app.ProgressDialog;
import android.content.Context;

import com.sss.generaltemplate.R;


public class LoaderUtils {
    private static ProgressDialog progress;

    public static void show(Context context) {
        try {
            progress = new ProgressDialog(context);
            progress.setMessage(context.getString(R.string.loading));
            progress.setCancelable(false);
            progress.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hide() {
        try {
            progress.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
