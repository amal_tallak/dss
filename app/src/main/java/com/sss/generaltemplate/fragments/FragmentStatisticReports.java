package com.sss.generaltemplate.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sss.generaltemplate.R;
import com.sss.generaltemplate.activities.MainActivity;
import com.sss.generaltemplate.adapters.StatisticReportsAdapter;
import com.sss.generaltemplate.model.StatisticReport;


public class FragmentStatisticReports extends Fragment {

    RecyclerView reportsRV;
    StatisticReport[] statisticReports;
    StatisticReportsAdapter statisticReportsAdapter;
    GridLayoutManager gridLayoutManager;
    public FragmentStatisticReports() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentStatisticReports(StatisticReport[] statisticReports) {
        this.statisticReports = statisticReports;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_statistic_reports, container, false);
        initLayoutElements(view);
        listAdapter();
        return view;
    }

    public void initLayoutElements(View view) {
        ((MainActivity) getActivity()).bottomPage.setVisibility(View.VISIBLE);
        reportsRV = view.findViewById(R.id.reports_rv);
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
    }
    public void listAdapter() {
        if (statisticReports.length > 0) {
            statisticReportsAdapter = new StatisticReportsAdapter(getContext(), statisticReports);
            reportsRV.setAdapter(statisticReportsAdapter);
            reportsRV.setLayoutManager(gridLayoutManager);

        } else
            Toast.makeText(getContext(), "adapter error", Toast.LENGTH_LONG).show();
    }
}
