package com.sss.generaltemplate.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.sayantan.advancedspinner.MultiSpinnerListener;
import com.sayantan.advancedspinner.SpinnerListener;
import com.sss.generaltemplate.R;
import com.sss.generaltemplate.activities.MainActivity;
import com.sss.generaltemplate.model.FactTable;
import com.sss.generaltemplate.model.ReportTable;
import com.sss.generaltemplate.model.StatisticTable;
import com.sss.generaltemplate.model.StatisticTableDimension;
import com.sss.generaltemplate.services.FetchStatisticsSevrice;

import java.util.ArrayList;
import java.util.List;

public class FragmentStatistic extends Fragment {

    com.sayantan.advancedspinner.MultiSpinner dimensionsSP;
    com.sayantan.advancedspinner.SingleSpinner statisticSP;
    Button showBtn;
    FragmentChartDetails fragmentChartDetails;
    FragmentShowChart fragmentShowChart;
    ReportTable[] reportTables;
    StatisticTable statisticTables;
    FactTable factTable;
    StatisticTableDimension[] dimensions;
    ArrayList<String> dimens;
    ArrayList<String> multiSPItems =new ArrayList<>();

    FetchStatisticsSevrice fetchStatisticsSevrice;
//    Chart chart;
//    HIXAxis hixAxis;
//    HIYAxis hiyAxis;

    public FragmentStatistic() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentStatistic(ReportTable[] reportTables) {
        this.reportTables = reportTables;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_statistic, container, false);

        initLayoutElements(view);
        getElements();
//        initServices();
//        chart.initChart();

        return view;
    }

    public void initLayoutElements(View view) {
        ((MainActivity) getActivity()).bottomPage.setVisibility(View.VISIBLE);
        dimensionsSP = view.findViewById(R.id.dimensions_sp);
        statisticSP = view.findViewById(R.id.statistic_sp);
        showBtn = view.findViewById(R.id.show_button);
        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentChartDetails = new FragmentChartDetails(factTable, dimens);
                ((MainActivity) getActivity()).setFragment(fragmentChartDetails);
            }
        });

    }

    public void getElements() {
        List<String> s = new ArrayList<>();
        for (int i = 0; i < reportTables.length; i++) {
            s.add(reportTables[i].getStatisticTable().getName());
        }
        statisticSP.setSpinnerList(s);
        dimensionsSP.setSpinnerList(multiSPItems);

        statisticSP.addOnItemChoosenListener(new SpinnerListener() {
            @Override
            public void onItemChoosen(final String s, int position) {
                if (position != -1) {
                    dimensionsSP.setSpinnerList(multiSPItems);
                    statisticTables = reportTables[position].getStatisticTable();
                    factTable = statisticTables.getFactTable();
                    dimensions = statisticTables.getStatisticTableDimensions();
                    multiSPItems = new ArrayList<>();
                    for (int i = 0; i < dimensions.length; i++) {
                        multiSPItems.add(dimensions[i].getDimension().getName());
                    }
                    dimensionsSP.setSpinnerList(multiSPItems);
                    dimensionsSP.addOnItemsSelectedListener(new MultiSpinnerListener() {
                        @Override
                        public void onItemsSelected(List<String> list, boolean[] booleans) {
                            if (booleans != null) {
                                dimens = new ArrayList<>();
                                for (int i = 0; i < booleans.length; i++) {
                                    if ((booleans[i] == true) && (dimens.size() >= 0) && (dimens.size() < 2)) {
                                        dimens.add(dimensions[i].getDimension().getName());
                                    } else if ((booleans[i] == true) && (dimens.size() >= 2)) {
                                        dimens.clear();
                                        Toast.makeText(getContext(), "can't choose more than two dimensions", Toast.LENGTH_LONG).show();
                                        break;
                                    }
                                }
                                if (dimens.size() > 0) {
                                    showBtn.setEnabled(true);
                                }
                            }
                        }
                    });
                }

            }
        });



    }


//    public void initServices() {
//        this.fetchStatisticsSevrice = APIUtils.getStatisticsSevrice();
//    }

//    public void callStatisticsSrvice(ArrayList<String> dimens) {
//        LoaderUtils.show(getContext());
//        int factTableId = factTable.getId();
//        final ArrayList<String> finalDimens = dimens;
//        fetchStatisticsSevrice.getStatistics(factTableId, Global.convertArrayToString(dimens), Global.token).enqueue(new Callback<JsonElement>() {
//            @Override
//            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
//                LoaderUtils.hide();
//
//                if (response.isSuccessful()) {
//                    if ((boolean) response.body().getAsJsonObject().get("returnCode").toString().contains("Success")) {
//                        ArrayList arrayListX = new ArrayList();
//                        ArrayList arrayListY = new ArrayList();
//                        ArrayList arrayListZ = new ArrayList();
//                        ArrayList arrayListYear = new ArrayList();
//
//                        JsonArray jsonArray = response.body().getAsJsonObject().get("list").getAsJsonArray();
//                        for (int i = 0; i < jsonArray.size(); i++) {
//                            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
//                            Integer year = jsonObject.get("year").getAsInt();
//                            Integer y = jsonObject.get("recCount").getAsInt();
//                            arrayListY.add(y);
//                            arrayListYear.add(year);
//                            for (int j = 0; j < finalDimens.size(); j++) {
//                                String temp = "";
//                                if (jsonObject.has(finalDimens.get(j))) {
//                                    if (Global.lang.equals("en")) {
//                                        temp = jsonObject.get(finalDimens.get(j)).getAsJsonObject().get("englishName").getAsString();
//                                    } else {
//                                        temp = jsonObject.get(finalDimens.get(j)).getAsJsonObject().get("arabicName").getAsString();
//                                    }
//                                }
//                                if (j == 0) {
//                                    arrayListX.add(temp);
//                                } else if (j == 1) {
//                                    arrayListZ.add(temp);
//                                }
//                            }
//                        }
////                        if (arrayListZ == null)
//                        chart.viewChart(arrayListX, arrayListY, arrayListZ, arrayListYear, "", finalDimens);
////
//
//                    }
//                } else {
//                    Toast.makeText(getContext(), "onResponse__Failed", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonElement> call, Throwable t) {
//                LoaderUtils.hide();
//                Toast.makeText(getContext(), "onFailure", Toast.LENGTH_LONG).show();
//
//            }
//        });
//    }


}
