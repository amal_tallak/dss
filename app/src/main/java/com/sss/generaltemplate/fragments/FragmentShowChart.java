package com.sss.generaltemplate.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sss.generaltemplate.Chart;
import com.sss.generaltemplate.activities.MainActivity;
import com.sss.generaltemplate.adapters.YearsAdapter;
import com.sss.generaltemplate.model.FactTable;
import com.sss.generaltemplate.services.FetchStatisticsSevrice;
import com.sss.generaltemplate.utils.APIUtils;
import com.sss.generaltemplate.utils.Global;
import com.sss.generaltemplate.utils.LoaderUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sss.generaltemplate.R.drawable;
import static com.sss.generaltemplate.R.id;
import static com.sss.generaltemplate.R.layout;

public class FragmentShowChart extends Fragment {

    FetchStatisticsSevrice fetchStatisticsSevrice;
    Chart chart;
    FactTable factTable;
    ArrayList<String> dimens;
    public Integer year = null;
    Response<JsonElement> chartData;
    ArrayList arrayListYear = new ArrayList();
    public ArrayList<Integer> listYear;
    public ArrayList<Integer> listYearMin = new ArrayList<>();
    com.sayantan.advancedspinner.SingleSpinner yearsSP;
    SeekBar seekBar;
    TextView seekBarValue;
    RecyclerView yearsRV;
    YearsAdapter yearsAdapter;
    GridLayoutManager gridLayoutManager;
    MyCountDown myCountDown;
    ImageButton startBtn, stopBtn,chartTypeBtn;
    ToggleButton toggleBtn;
    Boolean isPaused, isStoped;
    long remainingTime;
    String type="column";

    public FragmentShowChart() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentShowChart(FactTable factTable, ArrayList<String> dimens) {

        this.factTable = factTable;
        this.dimens = dimens;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(layout.fragment_show_chart, container, false);

        initLayoutElements(view);
        chart.initChart();
        initServices();
        callStatisticsService(dimens);

        return view;
    }

    public void initLayoutElements(View view) {
        ((MainActivity) getActivity()).bottomPage.setVisibility(View.GONE);
        chart = new Chart(view);
        seekBar = view.findViewById(id.seekBar);
        yearsRV = view.findViewById(id.years_rv);
        startBtn = view.findViewById(id.start_btn);
        stopBtn = view.findViewById(id.stop_btn);
        toggleBtn = view.findViewById(id.toggle_btn);
        chartTypeBtn = view.findViewById(id.chart_type_btn);
        isPaused = false;
        isStoped = false;

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSeekbar();
            }
        });

        toggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSeekbar();
            }
        });
        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopSeekbar();
            }
        });
        chartTypeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//                builderSingle.setIcon(R.drawable.ic_launcher);
                builder.setTitle("Select Chart Type");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.select_dialog_singlechoice);
                arrayAdapter.add("column");
                arrayAdapter.add("bar");

                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        type=arrayAdapter.getItem(which);
                        buildChart(year);
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
    }

    public void initServices() {
        this.fetchStatisticsSevrice = APIUtils.getStatisticsSevrice();
    }

    public void callStatisticsService(ArrayList<String> dimens) {
        LoaderUtils.show(getContext());
        int factTableId = factTable.getId();
        fetchStatisticsSevrice.getStatistics(factTableId, Global.convertArrayToString(dimens), Global.token).enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                LoaderUtils.hide();

                if (response.isSuccessful()) {
                    if ((boolean) response.body().getAsJsonObject().get("returnCode").toString().contains("Success")) {
                        chartData = response;
                        listYear = new ArrayList();
                        JsonArray jsonArray = chartData.body().getAsJsonObject().get("list").getAsJsonArray();
                        for (int i = 0; i < jsonArray.size(); i++) {
                            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                            Integer year = jsonObject.get("year").getAsInt();
                            arrayListYear.add(year);
                            if (listYear.indexOf(arrayListYear.get(i)) == -1) {
                                listYear.add((Integer) arrayListYear.get(i));
                            }
                        }

                        yearsAdapter = new YearsAdapter(listYear);
                        gridLayoutManager = new GridLayoutManager(getContext(), listYear.size(), GridLayoutManager.VERTICAL, false);
                        yearsRV.setAdapter(yearsAdapter);
                        yearsRV.setLayoutManager(gridLayoutManager);
                        seekBar.setMax(listYear.size());
                        startSeekbar();
                        yearsAdapter.setOnItemClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                YearsAdapter.MyViewHolder myViewHolder = (YearsAdapter.MyViewHolder) view.getTag();
                                int position = myViewHolder.getAdapterPosition();
                                year = listYear.get(position);
                                seekBar.setProgress(position);
                                toggleBtn.setChecked(true);
                                toggleBtn.setBackgroundDrawable(getResources().getDrawable(drawable.ic_resume));
                                toggleSeekbar();
                                remainingTime = ((listYear.size() - position) * 3000) - 3000;
                                buildChart(year);
                            }
                        });

                    }
                } else {
                    Toast.makeText(getContext(), "onResponse__Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                LoaderUtils.hide();
                Toast.makeText(getContext(), "onFailure", Toast.LENGTH_LONG).show();

            }
        });
    }

    public void buildChart(Integer year) {
        ArrayList arrayListX = new ArrayList();
        ArrayList arrayListY = new ArrayList();
        ArrayList arrayListZ = new ArrayList();
        JsonArray jsonArray = chartData.body().getAsJsonObject().get("list").getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
            Integer jsonYear = jsonObject.get("year").getAsInt();
            if (year != null && !jsonYear.equals(year)) continue;
//            arrayListYear = new ArrayList();
            Integer y = jsonObject.get("recCount").getAsInt();
            arrayListY.add(y);
//            arrayListYear.add(year);
            for (int j = 0; j < dimens.size(); j++) {
                String temp = "";
                if (jsonObject.has(dimens.get(j))) {
                    if (Global.lang.equals("en")) {
                        temp = jsonObject.get(dimens.get(j)).getAsJsonObject().get("englishName").getAsString();
                    } else {
                        temp = jsonObject.get(dimens.get(j)).getAsJsonObject().get("arabicName").getAsString();
                    }
                }
                if (j == 0) {
                    arrayListX.add(temp);
                } else if (j == 1) {
                    arrayListZ.add(temp);
                }
            }
        }
        chart.viewChart(arrayListX, arrayListY, arrayListZ, dimens,type);
    }

    public class MyCountDown extends CountDownTimer {

        public MyCountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            seekBar.setProgress(0);
            seekBar.setMax(listYear.size() - 1);
        }

        @Override
        public void onFinish() {
            if (isStoped == true)
                stopSeekbar();
            else
            if (isPaused == true)
                toggleSeekbar();
            else
                startSeekbar();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (isPaused || isStoped) {
//                toggleBtn.setBackgroundDrawable(getResources().getDrawable(drawable.ic_resume));
                cancel();
            } else {
                int progress = (int) (millisUntilFinished / 3000);
                remainingTime = millisUntilFinished - 3000;
                int seekBarProgress = seekBar.getMax() - progress;
                seekBar.setProgress(seekBarProgress);
                year = listYear.get(seekBarProgress);
                buildChart(year);
            }
        }
    }

    public void startSeekbar() {
        startBtn.setVisibility(View.GONE);
        stopBtn.setVisibility(View.VISIBLE);
        toggleBtn.setVisibility(View.VISIBLE);
        isPaused = false;
        isStoped = false;
        toggleBtn.setBackgroundDrawable(getResources().getDrawable(drawable.ic_pause));
        myCountDown = new MyCountDown(listYear.size() * 3000, 3000);
        myCountDown.start();
    }

    public void toggleSeekbar() {
        if (toggleBtn.isChecked()) {
            isPaused = true;
            toggleBtn.setBackgroundDrawable(getResources().getDrawable(drawable.ic_resume));
        } else {
            isPaused = false;
            toggleBtn.setBackgroundDrawable(getResources().getDrawable(drawable.ic_pause));
            myCountDown = new MyCountDown(remainingTime, 3000);
            myCountDown.start();
        }
    }

    public void stopSeekbar() {
        isStoped = true;
        startBtn.setVisibility(View.VISIBLE);
        stopBtn.setVisibility(View.GONE);
        toggleBtn.setVisibility(View.GONE);
        seekBar.setProgress(0);
    }


}