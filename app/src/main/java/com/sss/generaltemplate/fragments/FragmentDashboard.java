package com.sss.generaltemplate.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.sss.generaltemplate.PagerIndicatorDecoration;
import com.sss.generaltemplate.R;
import com.sss.generaltemplate.activities.MainActivity;
import com.sss.generaltemplate.adapters.SubSystemAdapter;
import com.sss.generaltemplate.model.DashboardItem;
import com.sss.generaltemplate.model.SubSystem;
import com.sss.generaltemplate.services.FetchSubSystemsSevrice;
import com.sss.generaltemplate.utils.APIUtils;
import com.sss.generaltemplate.utils.Global;
import com.sss.generaltemplate.utils.LoaderUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentDashboard extends Fragment {


    FetchSubSystemsSevrice fetchSubSystemsSevrice;
    SubSystem[] subSystems;
    GridLayoutManager gridLayoutManager;
    RecyclerView subSystemRV;
    SubSystemAdapter subSystemAdapter;

    public FragmentDashboard() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        initLayoutElements(view);
        initServices();
        callSubSystemsSevrice();

        return view;
    }

    public void initLayoutElements(View view) {
        ((MainActivity) getActivity()).bottomPage.setVisibility(View.VISIBLE);

        subSystemRV = view.findViewById(R.id.sub_system_rv);
        subSystemRV.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);

    }

    public void initServices() {
        this.fetchSubSystemsSevrice = APIUtils.getSubSystemsSevrice();
    }

    public void callSubSystemsSevrice() {

        LoaderUtils.show(getContext());

        fetchSubSystemsSevrice.getReports(Global.token).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                LoaderUtils.hide();

                if (response.isSuccessful()) {
                    if ((boolean) response.body().getAsJsonObject().get("returnCode").toString().contains("Success")) {
                        subSystems = SubSystem.getSubSystemFromJson(response.body().getAsJsonObject().get("list").getAsJsonArray());
                        listAdapter();
                    }
                } else {
                    Toast.makeText(getContext(), "onResponse__Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                LoaderUtils.hide();
                Toast.makeText(getContext(), "onFailure", Toast.LENGTH_LONG).show();

            }
        });
    }


    public void listAdapter() {
        if (subSystems.length > 0) {
            int length = subSystems.length % 6 == 0 ? (subSystems.length / 6) : ((subSystems.length / 6) + 1);
            DashboardItem[] dashboardItems = new DashboardItem[length];
            int j = -1;
            for (int i = 0; i < subSystems.length; i++) {
                if (i % 6 == 0) {
                    j++;
                    dashboardItems[j] = new DashboardItem();
                    dashboardItems[j].getItems().add(subSystems[i]);
                } else {
                    dashboardItems[j].getItems().add(subSystems[i]);
                }
            }

            subSystemAdapter = new SubSystemAdapter(getContext(), dashboardItems);
            subSystemRV.setAdapter(subSystemAdapter);
            subSystemRV.setLayoutManager(gridLayoutManager);
            subSystemRV.addItemDecoration(new PagerIndicatorDecoration());
        } else
            Toast.makeText(getContext(), "adapter error", Toast.LENGTH_LONG).show();
    }
}
