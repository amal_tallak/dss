package com.sss.generaltemplate.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ToxicBakery.viewpager.transforms.ZoomOutSlideTransformer;
import com.sss.generaltemplate.R;
import com.sss.generaltemplate.activities.MainActivity;
import com.sss.generaltemplate.adapters.PageAdapter;
import com.sss.generaltemplate.model.FactTable;

import java.util.ArrayList;

public class FragmentChartDetails extends Fragment {

    ViewPager viewPager;
    PageAdapter pageAdapter;
    FactTable factTable;
    ArrayList<String> dimens;

    public FragmentChartDetails() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentChartDetails(FactTable factTable, ArrayList<String> dimens) {
        this.factTable = factTable;
        this.dimens = dimens;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chart_details, container, false);
//        ((MainActivity) getActivity()).bottomNavigationView.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).setPageTitle(" ");
        initLayoutElements(view);
        if (this.pageAdapter == null)
            pageAdapter = new PageAdapter(getChildFragmentManager(), getContext(),factTable,dimens);
        viewPager.setAdapter(pageAdapter);
        viewPager.setPageTransformer(true, new ZoomOutSlideTransformer());
        return view;
    }

    public void initLayoutElements(View view) {
        viewPager = view.findViewById(R.id.viewpager);
    }


}
